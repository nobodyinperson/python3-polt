__version__ = "1.1.0"

POLT_PARSER_ENTRY_POINT = "polt.parser"
"""
The entry point name for :class:`Parser` extensions
"""

POLT_ANIMATOR_ENTRY_POINT = "polt.animator"
"""
The entry point name for :class:`Animator` extensions
"""

POLT_FILTER_ENTRY_POINT = "polt.filter"
"""
The entry point name for :class:`Filter` extensions
"""
