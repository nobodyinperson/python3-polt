Customizing ``polt``
====================

:mod:`polt` aims to be very customizable. You can use :ref:`parsers
customization`, :ref:`animators customization` and :any:`filters customization`
if the :ref:`parsers built-in`, :ref:`animators built-in` or :ref:`filters
built-in` don't do it for you.

Plot Appearance
+++++++++++++++

As :mod:`polt` uses :mod:`matplotlib`, the plots can be customized like any
other :mod:`matplotlib` plot via the ``matplotlibrc`` configuration file. See
`Customizing Matplotlib`_ for the official documentation.

Just to mention a couple of possibilities, you may put the following
``matplotlibrc`` file in the directory you are launching the :doc:`cli` from:

.. code:: python

    # display a grid
    axes.grid: True
    # display the grid behind the lines
    axes.axisbelow: True
    # Change the timezone in which the times will be displayed.
    # Using a bogus timezone string will cause matplotlib to use the current
    # timezone. 'local' is a nonexistant timezone.
    timezone: local
    # shrink the legend
    legend.fontsize: xx-small

.. _Customizing Matplotlib: https://matplotlib.org/tutorials/introductory/customizing.html?highlight=customizing#the-matplotlibrc-file

