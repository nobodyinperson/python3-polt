Quickstart
==========

Here are some examples to get you started with :mod:`polt`.

Just a Stream of Numbers
++++++++++++++++++++++++

If you just want to plot data like this (consecutive numbers, e.g. separated by lines):

.. code-block:: sh

    # generate this 10Hz-data stream for example via: polt generate -r 10
    -0.6152724135703695
    -0.42631509753041485
    -0.7636894349665775
    -0.38741616000586143
    -0.38131260109556586
    -0.6311144058504703
    0.13797279616200409
    0.47139959088898253
    -0.5891334488488134
    0.01998982713304276
    ...

... Just `pipe it <https://en.wikipedia.org/wiki/Pipeline_%28Unix%29>`_ into ``polt live``:


.. code-block:: sh

    (your-process-generating-a-stream-of-numbers) | polt live

CSV
+++

Here are some examples for parsing a `CSV <https://en.wikipedia.org/wiki/Comma-separated_values>`_ data stream.

Tailing a CSV File
------------------

.. code-block:: sh

    # 'tail -f' by itself just outputs the last lines and might not include the first CSV header line
    # So output the first line of the file (the header), then keep outputting new incoming lines
    (head -n1 file.csv; tail -fn0 file.csv) | polt add-source -p csv live

Selecting CSV columns
---------------------

.. code-block:: sh

    # only use columns "temp", "RH" and "wind"
    (head -n1 file.csv; tail -fn0 file.csv) | polt add-source -p csv -o only-columns=temp,RH,wind  live

When there is no CSV header
---------------------------

.. code-block:: sh

    # Add a virtual header row (just numbers - you can set the 100 to the actual number of columns)
    (seq 100; tail -fn0 file.csv) | polt add-source -p csv live

    # only use columns 2,4 and 5
    (seq 100; tail -fn0 file.csv) | polt add-source -p csv -o only-columns=2,4,5 live
