Command-Line Interface
======================

:mod:`polt` is designed to be a universal live-plotting tool for displaying
numerical data from multiple sources simultaneously. To make this functionality
easily available, a :doc:`cli` is provided.

Invocation
++++++++++

You can access :mod:`polt`'s :doc:`cli` by running

.. code-block:: sh

   polt

If running the above fails with something sounding like ``command not found:
polt``, you may either always run ``python3 -m polt`` instead of a plain
``polt`` or add the directory ``~/.local/bin`` to your ``PATH`` environment
variable by appending the following line to your shell's configuration
(probabily ``.bashrc`` in your home directory):

.. code-block:: sh

   export PATH="$HOME/.local/bin:$PATH"

The :doc:`cli` accepts commands and options and acts like a batch processor.
Multiple commands may be specified and they will be processed in the given
order.

Typically, invoking the :doc:`cli` consists of :ref:`cli adding sources`,
possibly :ref:`cli adding filters` and finally :ref:`cli live plotting`:

.. code-block:: sh

   polt add-source [SOURCE1-SETUP] ... add-filter [FILTER1-SETUP] ... live [PLOT-OPTIONS]

.. _cli generating data:

Generating Sample Data
++++++++++++++++++++++

To simplify demonstration, :mod:`polt` features generation of sample data via
the ``generate`` command. It can be used to generate `CSV <csv on
wikipedia_>`_ output with configurable columns:

.. code-block:: sh

   polt generate -c random -c "exponential=exp(3)" -c "normal=gauss(1,1)" | head
   # random,exponential,normal
   # 0.19236379321481523,0.11268430565570854,1.630175106516405
   # 0.30934369945221707,0.0679612382587536,1.578175939540361
   # 0.568986398081002,0.44355656114007047,1.3301134196920474
   # 0.15490505982693947,1.2347115017233534,2.147912951860432
   # 0.32026424261790665,0.23407180590311674,0.558304286547928
   # 0.47510801502777944,0.7731647408998108,0.2109845725674565
   # 0.5501377384406899,0.07665167594148191,1.6400343631915681
   # 0.26309742449195406,0.23447999895622032,0.4910447500187979
   # 0.6165970208950966,0.4455067802697222,1.000203903386154

All functions in :mod:`random` are suppored as data generators. The :ref:`cli
help` (``polt generate --help``) contains a detailed description of the
available options. To mention a few possibilities it is possible to limit the
data rate (output rows per second), the amount of data (in terms of rows and
time) and the csv format.

.. _csv on wikipedia: https://en.wikipedia.org/wiki/Comma-separated_values

.. _cli managing configuration:

Managing Configuration
++++++++++++++++++++++

The ``config`` command can be used to query, import or save the current
configuration. Without arguments, it just dumps the current configuration:

.. code-block:: sh

   polt config
   # no output as nothing has been configured yet

   polt add-source -c- config
   # [source:stdin]
   # command = -
   # parser = polt.parser.numberparser.NumberParser

   # save the configuration from the command-line to file
   polt add-source -c- config --save my-polt-config.conf

   # read the configuration from a file
   polt config --read my-polt-config.conf config
   # [source:stdin]
   # command = -
   # parser = polt.parser.numberparser.NumberParser

The :doc:`cli` is designed to make command-line and configuration transparent,
i.e. when you set up your :mod:`polt` configuration on the command-line and
save the configuration as last command, you should be able to read the same
configuration back with a ``config --read`` and spare yourself the cumbersome
long one-liner.

The configuration files ``~/.config/polt/polt.conf`` and ``.polt.conf`` in the
current working directory are read by default unless the ``--no-config`` option
was given. So if you find yourself always specifying the same command-line
options, check the configuration with ``config`` and apply the relevant parts
to one of these default configuration files to spare yourself the typing.


.. _cli adding sources:

Adding Data Sources
+++++++++++++++++++

The ``add-source`` command is used to add new data sources. For example, if you
have a process ``myprocess --foo=bar`` which you can execute from the shell
outputting numbers line-by-line, you would set it up like this:


.. code-block:: sh

    polt add-source --cmd "myprocess --foo=bar" --parser numbers
    # if you append the 'config' command, you can see the configuration:
    # [source:myprocess --foo=bar]
    # command = myprocess--foo=bar
    # parser = polt.parser.numberparser.NumberParser

Leaving out ``--cmd`` or specifying a dash (``-``) as command means using data
from standard input.

.. _cli adding sources specifying parser:

Specifying the Parser to Use
----------------------------

The ``--parser`` option to the ``add-source`` command can be used to select the
parser for the given source specified with ``--cmd``. :mod:`polt` ships with a
couple of :ref:`parsers built-in`.

.. _cli adding sources specifying parser options:

Specifying Parser Options
-------------------------

With the option ``--option ATTRIBUTE=VALUE`` of the ``add-source`` command it
is possible to set attributes on the parser. Internally, :func:`setattr` is
used after `snake-casing <https://en.wikipedia.org/wiki/Snake_case>`_ the
attribute name. The value will always be a :class:`str` so the parser class
itself has to make sure to convert the value accordingly. This is very handy to
configure parsers and can of course also be used with :ref:`parsers
customization`.


.. _cli adding sources specifying source encoding:

Specifying the Source Encoding
------------------------------

By default, the :ref:`cli live plotting` will open the configured source
command in plain bytes mode. This means that the :any:`Parser.f` will yield
:class:`bytes` data instead of :class:`str` which might not be desired. To fix
this, determine the encoding of your source and specify it with the
``--encoding`` option. An example of this can be seen in the :ref:`parsers
customization` section.

.. _cli adding sources limit data rate:

Limiting the Recieved Data Rate
-------------------------------

It may happen that your configured source command outputs data too fast for the
:ref:`cli live plotting` to catch up. Most likely, the below will not display
any data but just leave a blank plot window:

.. code-block:: sh

    # by default, `polt generate` outputs as fast as possible
	polt generate | polt live

In this case, it can help to limit the recieved data rate with the
:any:`Streamer.max_rate` property which can be set with the ``--max-rate``
option to the ``add-source`` command.  It takes a rate (in datasets per second
= `Hertz <hertz on wikipedia_>`_) as argument and causes data faster than this
rate to be dropped. With this option, we can limit the data rate to - say - 50
Hz to make the above command work:

.. code-block:: sh

	polt generate | polt add-source --cmd - --max-rate 50 live

.. image:: images/polt-random-numbers-limit-rate.png

.. _hertz on wikipedia: https://en.wikipedia.org/wiki/Hertz

Fine-Tuning the Buffer Time
---------------------------

By default, parsed data is immediately appended to the shared buffer (see
:ref:`cli live plotting data flow`). If the input stream is fast, this may be
inefficient as appending to the shared buffer is slow. The :any:`Streamer` can
buffer parsed data itself for a specified amount of time
(:any:`Streamer.flush_after`) before the data is flushed to the shared buffer.
You can set this option via ``--flush-after``.

.. _cli adding filters:

Adding Data Filters
+++++++++++++++++++

The ``add-filter`` command is used to add :doc:`filters`.

.. code-block:: sh

    polt add-filter -f rename

.. _cli adding filters specifying filter options:

Specifying Filter Options
-------------------------

Similiar to :ref:`cli adding sources specifying parser options` it is possible
to set options on the filter with the ``--option ATTRIBUTE=VALUE`` of the
``add-filter`` command. Internally, :func:`setattr` is used after
`snake-casing <https://en.wikipedia.org/wiki/Snake_case>`_ the attribute name.
The value will always be a :class:`str` so the filter class itself has to make
sure to convert the value accordingly. This is very handy to configure filters
and can of course also be used with :ref:`filters customization`.

.. _cli live plotting:

Live Data Plotting
++++++++++++++++++

Based on the current configuration the ``live`` command starts the parsing
threads and launches one of the :doc:`animators` which is responsible for
displaying the recieved data dynamically.

.. _cli live plotting data flow:

Data Flow
---------

Internally, the ``live`` command sets up the following structure:

.. mermaid::

    graph TD;

        subgraph Manager Process
        buffer((Shared Buffer))
        end

        subgraph Streamer Thread 1
        source1[stdin] -- parsed by --> parser1(NumberParser)
        parser1 -. appends to .-> buffer1((Buffer))
        buffer1 -. flushed to .-> buffer
        end

        subgraph Streamer Thread 2
        source2[tail -f file.csv] -- parsed by --> parser2(CsvParser)
        parser2 -. appends to .-> buffer2((Buffer))
        buffer2 -. flushed to .-> buffer
        end

        subgraph Streamer Thread 3
        source3[myprocess --foo=bar] -- parsed by --> parser3(MyCustomParser)
        parser3 -. appends to .-> buffer3((Buffer))
        buffer3 -. flushed to .-> buffer
        end

        subgraph Animator Process
        buffer -. read and modified by .-> filters(Filters)
        filters -. output displayed in .-> plot[Plot Window]
        end


.. note::

   The trained eye will notice that this structure indeed involves **three**
   separate processes. This might seem a little overkill for a seemingly simple
   task as plotting a couple of numbers. The reason :mod:`polt` was designed
   like this is that in order to be able to parse multiple sources
   simultaneously some level of concurrency (i.e. using threads or processes)
   is needed to due to the blocking nature of file objects in Python.
   Unfortunately, :mod:`matplotlib` does not play nicely with threads (see the
   StackOverflow question `Plotting with Matplotlib in Threads`_) unless one
   embeds :mod:`matplotlib` into a specific backend (like Gtk or Qt).  However,
   I wanted to keep **flexibility** high so fixing the backend was no option.
   What works is running :mod:`matplotlib` in another
   :class:`multiprocessing.Process`. This way, all
   :class:`polt.streamer.Streamer` instances can safely be run in the main
   process without disturbing the plot. The problem with this approach however
   is making the gathered parsed data available to the plot process. The most
   flexible approach to achieve this turned out to be a
   :class:`multiprocessing.Manager` process acting as communicator.

.. _Plotting with Matplotlib in Threads: https://stackoverflow.com/questions/19662906/plotting-with-matplotlib-in-threads

Default Setup
-------------

Without any options, the default configuration reads numbers from standard
input and uses the :any:`TimeLinesAnimator` for :ref:`animator displaying
lines`:

.. code-block:: sh

   polt generate -c walk --max-rate 20 | polt live

.. image:: images/polt-live-random-walk.png

.. _cli live specifying animator options:

Specifying Animator Options
---------------------------

Similar to :ref:`cli adding sources specifying parser options` it is possible
to set attributes on the animator with the option ``--option ATTRIBUTE=VALUE``
to the ``live`` command.  Internally, :func:`setattr` is used after
`snake-casing <https://en.wikipedia.org/wiki/Snake_case>`_ the attribute name.
The value will always be a :class:`str` so the animator class itself has to
make sure to convert the value accordingly. This is very handy to configure
animators and is of course also possible with :ref:`animators customization`.

.. _cli live modifying animation framerate:

Modifying the Animation Framerate
---------------------------------

Between two animation frame updates, the animator idles for
:any:`Animator.interval` milliseconds. The default is ``200`` milliseconds. You
can change this by specifying ``-o interval=INTERVAL``.

You may also specify ``-o max-fps=FRAMERATE`` if you would rather like to
specify a maximum framerate.

Pausing
-------

It is possible to pause/resume the animation by hitting the spacebar. Pausing
is useful to examine the graphs without the plot being changed constantly.

When paused, data is still being parsed and appended to the shared buffer but
not consumed by the animation. If instead you would rather like to drop data
during pause, specify the :any:`Animator.drop_on_pause` property to ``True``
via ``-o drop-on-pause=yes``.

.. _cli improving performance:

Improving Performance
+++++++++++++++++++++

.. note::

    :mod:`polt` is **not** built for speed! It is intended to be **easy to
    use** and **quick to setup**. If you need to display fast (i.e. rates
    higher than hundreds of Hz) data in real time, you might be better off with
    `PyQtGraph`_.


There are a couple of possibilities to improve the performance if :mod:`polt`
is performing badly (e.g. due to too-fast input streams):

- try :ref:`cli adding sources limit data rate`
- try :ref:`animator lines time frame`
- try :ref:`cli live modifying animation framerate`

.. _PyQtGraph: http://www.pyqtgraph.org

.. _cli help:

Help
++++

The help page of the :doc:`cli` can always be viewed by appending the
``--help`` option:

.. code-block:: sh

    # general help page
    polt --help
    # help page for the 'live' command
    polt live --help
    # help page for the 'add-source' command
    polt add-source --help
    # help page for the 'config' command
    polt config --help
    # help page for the 'generate' command
    polt generate --help

