Animators
=========

To get a list of available :doc:`animators` and their usable aliases, you may
use the :any:`EntryPointExtensions.get` function:

.. code:: python

   polt.extensions.EntryPointExtensions.get(polt.version.POLT_ANIMATOR_ENTRY_POINT, aliases=True)
   # yields something like
   {('hist',
     'HistAnimator',
     'polt.animator.hist.HistAnimator'): polt.animator.hist.HistAnimator,
    ('lines',
     'TimeLinesAnimator',
     'polt.animator.timelines.TimeLinesAnimator'): polt.animator.timelines.TimeLinesAnimator,
    ('spectrum',
     'SpectrumAnimator',
     'polt.animator.spectrum.SpectrumAnimator'): polt.animator.spectrum.SpectrumAnimator}



The default animator is the :any:`TimeLinesAnimator` which displays recieved
data as lines.

See :ref:`cli live specifying animator options` to learn how to specify
animator options from the :doc:`cli`.

.. _animators built-in:

Built-In Animators
++++++++++++++++++

:mod:`polt` ships with some built-in animators. You can also use
:ref:`animators customization`.

.. _animator displaying lines:

Displaying Data as Lines
------------------------

The :any:`TimeLinesAnimator` is a convenient :any:`Animator` displaying
recieved data as lines. For a full list of options, see its documentation
(:any:`TimeLinesAnimator`).

.. _animator lines extrapolate:

Extrapolating Data
..................

With the ``-o extrapolate=yes`` option values are continued constantly:

.. code-block:: sh

    echo 1 | polt live -o extrapolate=yes

.. image:: images/polt-live-extrapolate.png
    :alt: echo 1 | polt live -o extrapolate=yes

.. _animator lines time frame:

Limiting the Displayed Time Frame
.................................

It might be desired to only show ”the last couple of minutes” of data, not
everything from the very beginning on. The displayed time frame can be limited
with the :any:`TimeLinesAnimator.time_frame` property. For example, if you only
want to display the last 10 seconds of data, specify ``-o time-frame=10`` to
the :ref:`cli live plotting` command.

Splitting Data into Subplots
............................


The most mentionable option is probabily ``-o subplots-for=...`` which enables
splitting the recieved data up into multiple subplots. It is possible to sort
data by ``unit``, ``quantity``, ``parser``, ``key`` or ``nothing`` (which is
the default). For this to work the parsers need to provide this metadata. Check
the :doc:`parsers` section for how to achieve this with the built-in parsers.

We are going to use the following configuration using dummy ``echo`` processes
and the option for :ref:`animator lines extrapolate` to generate some sample
data:

.. code-block:: sh

   polt generate \
        -c "sensor1_temperature_celsius=uniform(20,25)" \
        -c "sensor1_pressure_hPa=uniform(990,1020)" \
        -c "sensor2_pressure_hPa=uniform(990,1020)" \
        -c "sensor3_humidity_percent=uniform(10,90)" \
        -c "sensor3_temperature_kelvin=uniform(0,300)" \
        --max-rate 5 \
        | polt \
            add-source -p csv -o name=Data -o header-regex=key-quantity-unit \
            live -o extrapolate=yes

.. image:: images/polt-live-5-random-sensors.png

Subplots for each Unit
''''''''''''''''''''''

.. code-block:: sh

   polt generate \
        -c "sensor1_temperature_celsius=uniform(20,25)" \
        -c "sensor1_pressure_hPa=uniform(990,1020)" \
        -c "sensor2_pressure_hPa=uniform(990,1020)" \
        -c "sensor3_humidity_percent=uniform(10,90)" \
        -c "sensor3_temperature_kelvin=uniform(0,300)" \
        --max-rate 5 \
        | polt \
            add-source -p csv -o name=Data -o header-regex=key-quantity-unit \
            live -o extrapolate=yes -o subplots-for=unit

.. image:: images/polt-live-5-random-sensors-by-unit.png


Subplots for each Quantity
''''''''''''''''''''''''''

.. code-block:: sh

   polt generate \
        -c "sensor1_temperature_celsius=uniform(20,25)" \
        -c "sensor1_pressure_hPa=uniform(990,1020)" \
        -c "sensor2_pressure_hPa=uniform(990,1020)" \
        -c "sensor3_humidity_percent=uniform(10,90)" \
        -c "sensor3_temperature_kelvin=uniform(0,300)" \
        --max-rate 5 \
        | polt \
            add-source -p csv -o name=Data -o header-regex=key-quantity-unit \
            live -o extrapolate=yes -o subplots-for=quantity

.. image:: images/polt-live-5-random-sensors-by-quantity.png


Equally it is possible to sort the data into subplots for each parser and each
key (which is the sensor in these examples).

.. _animators live histogram:

Displaying Live Histograms
--------------------------

The :any:`HistAnimator` can be used to display live histograms of the incoming
data.

.. code-block:: sh

    polt generate --max-rate 20 \
        -c "uniform=uniform(0,1)" -c "exponential=exp(3)" -c "gaussian=gauss(1,1)" \
        | polt \
            add-source -p csv -o name=Random \
            live -a hist -o normed=yes -o bins=30

.. image:: images/polt-hist-3-distributions.png

.. _animators live spectrum:

Displaying Live Spectra
-----------------------

The :any:`SpectrumAnimator` can be used to display live spectra of the incoming
data.

.. code-block:: sh

    polt \
        add-source -p csv -c "polt generate -c walk -r 20" \
        add-source -p csv -c "polt generate -c walk -r 25" \
        live -a spectrum -o time-frame=10

.. image:: images/polt-2-random-walk-spectra.png


.. hint::

   Click into the plot window and use the ``k`` and ``l`` keys to change the
   axes scale from linear to logarithmic.

.. _animators customization:

Custom Animators
++++++++++++++++

Similar to using :ref:`parsers customization` it is possible to use your
own :any:`Animator` which takes care of displaying the gathered data
dynamically.

For example you could implement an animator which plots scatter points
like this:

.. code:: python

    from polt.animator.subplots import SubPlotsAnimator
    from polt.utils import to_float, to_tuple
    from datetime import datetime

    class ScatterAnimator(SubPlotsAnimator):
        def update_figure(self, datasets):
            # remember the first time a dataset was recieved
            if not hasattr(self, "first_time"):
                self.first_time = datetime.utcnow()
            # use the first (and only) axes or create new
            ax = self.figure.axes[0] if self.figure.axes else self.add_axes()
            # loop over all recieved datasets
            for dataset in (datasets or tuple()):
                # get the time where the dataset was recieved
                time = dataset.pop("time_recieved_utc", datetime.utcnow())
                # get the actual data
                data = dataset.pop("data", {})
                for key, val in data.items():
                   # make sure the data is a tuple
                   for v in filter(
                       lambda x: x is not None,
                       map(to_float, to_tuple(val))
                   ):
                       ax.scatter(time, v)
            # make sure everything is pretty
            ax.set_xlim(left=self.first_time, right = datetime.utcnow())
            ax.autoscale_view(scalex=True)
            self.figure.autofmt_xdate()


Save this file to ``scatteranimator.py`` in the directory where you launch the
:doc:`cli` from. You can now use your custom animator like this:

.. code-block:: sh

   while true;do echo $RANDOM;sleep 1;done | polt live -a scatteranimator.ScatterAnimator


.. image:: images/polt-custom-scatter-animator.png


.. note::

   Note that this ``ScatterAnimator`` is very poorly optimized. For example
   it calls :any:`matplotlib.axes.Axes.set_xlim`,
   :any:`matplotlib.axes.Axes.autoscale_view` and
   :any:`matplotlib.figure.Figure.autofmt_xdate` on each iteration.

See :ref:`extending polt` to learn how to make your custom animator globally
available.
