Welcome to polt's documentation!
================================

:mod:`polt` is a Python package for live data visualization via
:mod:`matplotlib`. It includes a :doc:`cli` designed to be a universal tool to
plot data from various sources simulataneously. Check out the :ref:`cli live
plotting data flow` for an overview of how data is gathered, processed and
displayed.

.. note::

    :mod:`polt` is **not** built for speed! It is intended to be **easy to
    use** and **quick to setup**. If you need to display fast (i.e. rates
    higher than hundreds of Hz) data in real time, you might be better off with
    `PyQtGraph`_.

.. _PyQtGraph: http://www.pyqtgraph.org


``polt`` in action
++++++++++++++++++

You can use the :doc:`cli` to quickly visualize numbers from a process' output:

.. code-block:: sh

   polt generate -c walk --max-rate 20 | polt live

.. image:: images/polt-live-random-walk.png

But the :doc:`cli` can do more like reading data in different formate
from multiple sources and splitting the data into subplots:

.. code-block:: sh

   polt generate \
        -c "sensor1_temperature_celsius=uniform(20,25)" \
        -c "sensor1_pressure_hPa=uniform(990,1020)" \
        -c "sensor2_pressure_hPa=uniform(990,1020)" \
        -c "sensor3_humidity_percent=uniform(10,90)" \
        -c "sensor3_temperature_kelvin=uniform(0,300)" \
        --max-rate 5 \
        | polt \
            add-source -p csv -o name=Data -o header-regex=key-quantity-unit \
            live -o extrapolate=yes -o subplots-for=unit

.. image:: images/polt-live-5-random-sensors-by-unit.png

Check the :doc:`cli` documentation to learn how to use it properly.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   quickstart
   cli
   parsers
   filters
   animators
   customizing
   extending
   changelog
   api/modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
