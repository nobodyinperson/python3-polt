.. _extending polt:

Extending ``polt``
==================

There exist possibilities to extend :mod:`polt`'s capabilities.

Adding built-in Parsers, Animators and Filters with Aliases
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

It is possible to add :ref:`parsers customization`, :ref:`animators
customization` and :ref:`filters customization`. However, a more robust way of
making your parsers, animators and filters available to :mod:`polt` is to use
`Entry Points`_.  Package your class with :mod:`setuptools` and register your
parsers, animators and/or filters in the ``"polt.parser"``, ``"polt.animator"``
and ``"polt.filter"`` entry points, for example like this:

.. code:: python

   setup(
       ...
       entry_points = {
           "polt.parser":     ["myparser = mypackage.mymodule.MyParserClass"],
           "polt.animator": ["myanimator = mypackage.mymodule.MyAnimatorClass"]
           "polt.filter":     ["myfilter = mypackage.mymodule.MyFilterClass"]
       }
       ...
       )

When your package is installed correctly (e.g. via ``pip3 install``),
regardless of your working directory, your custom parser class
``MyParserclass`` can then be used with the ``"myparser"`` alias, your
custom animator ``MyAnimatorClass`` with the ``"myanimator"`` alias and your
custom filter class ``MyFilterClass`` with the ``myfilter`` alias.

.. _Entry Points: https://setuptools.readthedocs.io/en/latest/setuptools.html#dynamic-discovery-of-services-and-plugins

