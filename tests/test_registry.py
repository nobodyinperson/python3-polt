# system modules
import unittest

# internal modules
from polt.registry import FunctionRegistry

# external modules


class RegistryTest(unittest.TestCase):
    def test_call_registered_methods(self):
        class MyClass(FunctionRegistry):
            @FunctionRegistry.register_method("slot1")
            def slot1_method(self):
                return self

            @FunctionRegistry.register_classmethod("slot1")
            def slot1_classmethod(cls):
                return cls

            @FunctionRegistry.register_classmethod("slot1")
            def slot1_classmethod(cls):
                return cls

        @MyClass.register_function("slot1", 1)
        def testfunc(v):
            return v

        class MySubClass(FunctionRegistry):
            @FunctionRegistry.register_method("slot1", "qwer", kw="asdf")
            def slot1_method(self, a, kw=None, other_kw=None):
                return (self, a, kw, other_kw)

        obj = MyClass()
        subobj = MySubClass()

        self.assertDictEqual(
            obj.call_registered_functions("slot1"),
            {
                MyClass.slot1_method: obj,
                MyClass.slot1_classmethod: MyClass,
                testfunc: 1,
            },
        )
        self.assertDictEqual(
            subobj.call_registered_functions("slot1", other_kw=2),
            {MySubClass.slot1_method: (subobj, "qwer", "asdf", 2)},
        )
